<?php

/**
 * @file
 * Contains FeedsImageBase64WebTest.
 */

/**
 * Basic feeds image base64 test.
 */
class FeedsImageBase64WebTest extends FeedsMapperTestCase {
  /**
   * Describes this test.
   *
   * @return array
   *   Information about the test.
   */
  public static function getInfo() {
    return array(
      'name' => 'Import base64 images',
      'description' => '',
      'group' => 'Feeds Base64 images',
    );
  }

  /**
   * Overrides FeedsWebTestCase::setUp().
   */
  public function setUp($modules = array()) {
    $modules = array_merge($modules, array('feedsimagebase64', 'yvklibrary'));
    parent::setUp($modules);
  }

  /**
   * Test importing base64 images.
   */
  public function testBase64() {
    $typename = $this->createContentType(array(), array('images' => 'image'));

    // Create importer configuration.
    $this->createImporterConfiguration();
    $this->setPlugin('syndication', 'FeedsFileFetcher');
    $this->setPlugin('syndication', 'FeedsCSVParser');
    $this->setSettings('syndication', 'FeedsNodeProcessor', array('bundle' => $typename));
    $this->addMappings('syndication', array(
      0 => array(
        'source' => 'title',
        'target' => 'title',
      ),
      1 => array(
        'source' => 'timestamp',
        'target' => 'created',
      ),
      2 => array(
        'source' => 'image',
        'target' => 'field_images:base64',
      ),
    ));

    // Import CSV file.
    $this->importFile('syndication', $this->absolute() . '/' . drupal_get_path('module', 'feedsimagebase64') . '/tests/feeds/content.csv');
    $this->assertText('Created 2 nodes');

    // Check if files were created.
    $this->checkCreatedFiles('syndication', 'field_images');
  }

  /**
   * Test importing base64 images with file name specified.
   */
  public function testBase64WithFileName() {
    $typename = $this->createContentType(array(), array('images' => 'image'));

    // Create importer configuration.
    $this->createImporterConfiguration();
    $this->setPlugin('syndication', 'FeedsFileFetcher');
    $this->setPlugin('syndication', 'FeedsCSVParser');
    $this->setSettings('syndication', 'FeedsNodeProcessor', array('bundle' => $typename));
    $this->addMappings('syndication', array(
      0 => array(
        'source' => 'title',
        'target' => 'title',
      ),
      1 => array(
        'source' => 'timestamp',
        'target' => 'created',
      ),
      2 => array(
        'source' => 'file_name',
        'target' => 'field_images:base64_filename',
      ),
      3 => array(
        'source' => 'image',
        'target' => 'field_images:base64',
      ),
    ));

    // Import CSV file.
    $this->importFile('syndication', $this->absolute() . '/' . drupal_get_path('module', 'feedsimagebase64') . '/tests/feeds/content.csv');
    $this->assertText('Created 2 nodes');

    // Check if files were created and with the expected name.
    $files = array(
      'druplicon.png',
      'image_test.jpg',
    );
    $this->checkCreatedFiles('syndication', 'field_images', $files);
  }

  /**
   * Test if images are deleted when they are no longer in source.
   */
  public function testFileDeletion() {
    $typename = $this->createContentType(array(), array('images' => 'image'));

    // Create importer configuration.
    $this->createImporterConfiguration();
    $this->setPlugin('syndication', 'FeedsFileFetcher');
    $this->setPlugin('syndication', 'FeedsCSVParser');
    // Turn on "update existing" option to test clear out values.
    $this->setSettings('syndication', 'FeedsNodeProcessor', array('bundle' => $typename, 'update_existing' => 2));
    $this->addMappings('syndication', array(
      0 => array(
        'source' => 'title',
        'target' => 'title',
        'unique' => 1,
      ),
      1 => array(
        'source' => 'timestamp',
        'target' => 'created',
      ),
      2 => array(
        'source' => 'image',
        'target' => 'field_images:base64',
      ),
    ));

    // Import CSV file.
    $this->importFile('syndication', $this->absolute() . '/' . drupal_get_path('module', 'feedsimagebase64') . '/tests/feeds/content.csv');
    $this->assertText('Created 2 nodes');

    // Check if files were created.
    $files = $this->checkCreatedFiles('syndication', 'field_images');

    // Import CSV file without image data.
    $this->importFile('syndication', $this->absolute() . '/' . drupal_get_path('module', 'feedsimagebase64') . '/tests/feeds/content_no_image.csv');
    $this->assertText('Updated 2 nodes');
    // Check if files are removed as expected.
    foreach ($files as $filename) {
      $this->assertFalse(file_exists($filename), strtr('The file !name no longer exists.', array('!name' => $filename)));
    }
  }

  /**
   * Checks if files of entities exists.
   *
   * @param string $id
   *   The ID of the importer.
   * @param string $field_name
   *   The name of the field to check.
   * @param array $files
   *   (optional) The file names to check.
   *
   * @return array
   *   List of files that are expected to exist.
   */
  protected function checkCreatedFiles($id, $field_name, $files = array()) {
    $saved_files = array();

    $feeds_items = db_select('feeds_item')
      ->fields('feeds_item', array('entity_id'))
      ->condition('id', 'syndication')
      ->execute();

    foreach ($feeds_items as $feeds_item) {
      // Assert the file exists.
      $node = node_load($feeds_item->entity_id);
      $filename = $node->{$field_name}['und'][0]['uri'];
      $this->assertTrue(file_exists($filename), strtr('The file !name for node !nid was saved.', array('!name' => $filename, '!nid' => $feeds_item->entity_id)));
      $saved_files[] = $filename;

      // Assert the expected file name.
      if (count($files)) {
        $this->drupalGet('node/' . $feeds_item->entity_id . '/edit');
        $f = new FeedsEnclosure(array_shift($files), NULL);
        $this->assertRaw($f->getUrlEncodedValue());
      }
    }

    return $saved_files;
  }
}
